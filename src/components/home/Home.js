import FzElement from "../element/Element.vue";

export default {
    components: {FzElement},
    name: 'FzHome',
    data() {
        this.inputNumberElements = 1;
        this.inputTitleElements = "a";
        this.elements = [];
        return {
            elements: this.elements,
            inputNumberElements: this.inputNumberElements
        }
    },
    methods: {
        addElement: function (numberElements) {
            let existingElements = this.elements.length;
            for (let i = 0; i < numberElements; i++) {
                this.elements.push({
                    title: `Element ${existingElements + i}`,
                    description: `Description of ${existingElements + i}`
                });
            }
        },
        addElementsInput: function () {
            this.addElement(this.inputNumberElements);
        },
        changeElementTitles: function () {
            for (let element of this.elements)
                element.title = element.title + " " + this.inputTitleElements;
        }
    }
}