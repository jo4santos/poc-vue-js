export default {
    name: 'FzElement',
    props: ["title", "description"],
    data() {
        this.collapsed = true;
        return {
            collapsed: this.collapsed
        }
    },
    methods: {
        toggleCollapsed: function () {
            this.collapsed = !this.collapsed;
        }
    }
}