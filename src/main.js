import Vue from 'vue';
import FzHome from './components/home/Home.vue';
import FzElement from './components/element/Element.vue';

Vue.component('fz-home', FzHome);
Vue.component('fz-element', FzElement);

new Vue({
  el: '#fz-app'
})
